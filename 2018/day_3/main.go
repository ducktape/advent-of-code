// Find the area of overlapping points
package main

import (
	"bufio"
	"fmt"
	"os"
)

type point struct {
	x int
	y int
}

type design struct {
	id     int
	points []point
}

func main() {
	input_file := os.Args[1]
	f, err := os.Open(input_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "part1: %v\n", err)
		panic(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	points := make(map[point]bool)
	designs := []design{}
	for scanner.Scan() {
		var id, x_0, y_0, x_all, y_all int
		line := scanner.Text()
		fmt.Sscanf(line, "#%d @ %d,%d: %dx%d",
			&id, &x_0, &y_0, &x_all, &y_all)

		design_points := []point{}
		for i := 1; i <= x_all; i++ {
			for j := 1; j <= y_all; j++ {
				p := point{x_0 + i, y_0 + j}

				if _, ok := points[p]; ok {
					points[p] = true
				} else {
					points[p] = false
				}

				design_points = append(design_points, p)
			}
		}
		designs = append(designs, design{id, design_points})
	}
	// Part 1, number of squares with overlap
	seen := 0
	for _, v := range points {
		if v {
			seen++
		}
	}
	fmt.Printf("%v overlaps\n", seen)

	// Part 2 A design with no overlaps
DESIGN_LOOP:
	for _, design := range designs {
		for _, p := range design.points {
			if overlap, ok := points[p]; ok && overlap {
				continue DESIGN_LOOP
			}
		}
		fmt.Printf("Design ID %v has no overlaps\n", design.id)
	}
}
