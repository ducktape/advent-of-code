package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	input_file := os.Args[1]
	f, err := os.Open(input_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "part1: %v\n", err)
		panic(err)
	}
    defer f.Close()
	frequency := 0
	input := bufio.NewScanner(f)
	for input.Scan() {
		line := input.Text()
		operator := line[0]
		var value int
		value, err = strconv.Atoi(line[1:])
		switch operator {
		case '+':
			frequency += value
		case '-':
			frequency -= value
		}
	}
	fmt.Printf("Frequency: %v\n", frequency)
}
