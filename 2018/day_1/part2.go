package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	//"github.com/davecgh/go-spew/spew"
)

type arithmetic func(int, int) int

type Operation struct {
	op  arithmetic
	val int
}

func add(a, b int) int {
	return a + b
}

func subtract(a, b int) int {
	return a - b
}

func main() {
	input_file := os.Args[1]
	f, err := os.Open(input_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "part2: %v\n", err)
		panic(err)
	}
	input := bufio.NewScanner(f)
	frequency := 0

	// Read in the list of operations ... into a list
	var operations []Operation
	for input.Scan() {
		line := input.Text()
		var operation Operation
		var value int
		value, err = strconv.Atoi(line[1:])
		if err != nil {
			fmt.Fprintf(os.Stderr, "part2, %v\n", err)
			panic(err)
		}
		operation.val = value

		operator := line[0]
		switch operator {
		case '+':
			operation.op = add
		case '-':
			operation.op = subtract
		}
		operations = append(operations, operation)
	}
	//    spew.Dump(operations)
	// Create a map with seen values
	seen := make(map[int]bool)
	seen[frequency] = true

	// Begin computing frequency until a seen value shows up
	found := false
CALC:
	for _, operation := range operations {
		frequency = operation.op(frequency, operation.val)
		if _, ok := seen[frequency]; ok {
			found = true
			break
		}
		seen[frequency] = true
	}
	if found {
		fmt.Printf("Frequency: %v\n", frequency)
	} else {
		goto CALC
	}
}
