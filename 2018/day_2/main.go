// Get a pseudo checksum of a file by finding the product of 
// the number of doubles and triples found per line
package main

import (
    "fmt"
    "bufio"
    "os"
)

// Get the rune count from a line
func get_counts(line string) (bool, bool){
    counter := make(map[rune]int)
    double, triple := false, false
    for _, char := range line {
        counter[char]++
    }

    // Check for doubles
    for _, value := range counter {
        if value == 2 {
            double = true
        } else if value == 3 {
            triple = true
        }

        if double && triple {
            break
        }
    }
    return double, triple
}

func main() {
	input_file := os.Args[1]
	f, err := os.Open(input_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "part1: %v\n", err)
		panic(err)
	}

	input := bufio.NewScanner(f)
    doubles, triples := 0, 0
	for input.Scan() {
		line := input.Text()
        double, triple := get_counts(line)
        if double {
            doubles += 1
        }
        if triple {
            triples += 1
        }
	}
    product := doubles * triples
    fmt.Printf("Checksum: %v * %v = %v\n", doubles, triples, product)

}
