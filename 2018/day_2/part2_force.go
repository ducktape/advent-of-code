package main

import (
	"bufio"
	"fmt"
	"os"
    "unicode/utf8"
//	"github.com/davecgh/go-spew/spew"
)

func main() {
	input_file := os.Args[1]
	f, err := os.Open(input_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "part2: %v\n", err)
		panic(err)
	}

	input := bufio.NewScanner(f)
    
    words := []string{}
	for input.Scan() {
		line := input.Text()
        words = append(words, line)
	}

    // Find similar lines (off by 1)
    MAIN_LOOP: for _, word := range words {
        for _, possible := range words {
            // Assuming all words are the same length and ascii
            diffs := 0
            for i, char := range word {
                possible_char, _ := utf8.DecodeRuneInString(possible[i:])
                if char != possible_char {
                    diffs += 1
                }
            }
            if diffs == 1 {
                fmt.Printf("%s and\n%s\n", word, possible)
                break MAIN_LOOP
            }
        }
    }
    fmt.Printf("END\n")
}
