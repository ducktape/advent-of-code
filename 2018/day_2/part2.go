// Get a pseudo checksum of a file by finding the product of
// the number of doubles and triples found per line
package main

import (
	"bufio"
	"fmt"
	"os"
	"github.com/davecgh/go-spew/spew"
)

func find_similar(n node, word string) string {
    for i, char := range word {
    }
}

fund find(n node, word string) {

}

type node struct {
	char  rune
	end   bool
	word  string
	nexts []*node
}

func insert(n *node, c rune) *node {
    if n == nil {
        return &node{char: c}
    }
    for _, nodule := range n.nexts {
        if nodule.char == c {
            return nodule
        }
    }
    
    new_node := &node{char: c}
    n.nexts = append(n.nexts, new_node)
    return new_node
}

func main() {
	input_file := os.Args[1]
	f, err := os.Open(input_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "part2: %v\n", err)
		panic(err)
	}

	input := bufio.NewScanner(f)
    
    words := []string
    root := node{
		char: '*',
		end:  false,
	}
    root_ref := &root
	for input.Scan() {
		line := input.Text()
		current := root_ref

		// Add the word to my tree
		for _, char := range line {
            current = insert(current, char)  
		}
        current.end = true
        current.word = line
        words = append(words, line)
	}

    // Find similar lines (off by 1)
    for _, line := range words {
        similar = find_similar(root, line)
        if similar != nil {
            fmt.Printf("%s and %s", line, similar)
            break
        }
    }
    fmt.Println("End")
}
